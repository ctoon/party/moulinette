import os
import re
import sys
import shutil
import requests
import readline
import argparse
import subprocess
import youtube_dl

args = None
parser = argparse.ArgumentParser(description='Moulinette')
parser.add_argument('-d', '--download', help='Download file')
parser.add_argument('-i', '--input', help='Input filename')
parser.add_argument('-v', '--video-track', default='v:0', help='Video track')
parser.add_argument('-a', '--audio-track', default='a:0', help='Audio track')
parser.add_argument('-w', '--show', help='shoW short code')
parser.add_argument('-s', '--season', help='Season number')
parser.add_argument('-e', '--episode', help='Episode number')
parser.add_argument('-l', '--lang', default='EN', help='audio Language')
parser.add_argument('-p', '--profile', default='WEBDL', help='video Profile')
parser.add_argument('-k', '--encoder', help='video enKoder')
parser.add_argument('-n', '--interactive', action='store_true', help='use an iNteractive prompt')


def check_args():
    args = parser.parse_args()

    # Ask us question if we went interactive
    if args.interactive:
        d = input(f'Download URL [{args.download}]: ')
        if d.strip() != '' : args.download = d
        i = input(f'Input file [{args.input}]: ')
        if i.strip() != '' : args.input = i
        v = input(f'Video map [{args.video_track}]: ')
        if v.strip() != '' : args.video_track = v
        a = input(f'Audio map [{args.audio_track}]: ')
        if a.strip() != '' : args.audio_track = a
        w = input(f'Show [{args.show}]: ')
        if w.strip() != '' : args.show = w
        s = input(f'Season [{args.season}]: ')
        if s.strip() != '' : args.season = s
        e = input(f'Episode [{args.episode}]: ')
        if e.strip() != '' : args.episode = e
        l = input(f'Language [{args.lang}]: ')
        if l.strip() != '' : args.lang = l
        p = input(f'Profile [{args.profile}]: ')
        if p.strip() != '' : args.profile = p
        k = input(f'Encoder [{args.encoder}]: ')
        if k.strip() != '' : args.encoder = k

    # Check for required args
    if (args.input is None and args.download is None) or args.show is None or args.season is None or args.episode is None:
        print('We\'re missing important details here.')
        exit(3)

    return args


def download_mega(url):
    print('Mega is not yet supported')
    sys.exit(2)


def ydl_hook(d):
    global args

    # Once the download finished
    if d['status'] == 'finished':
        # We'll need to extract the final filename
        # from the parts it downloaded (like YTIDCODE.f123.mp4 => YTIDCODE.mp4)
        # this might not work at all, it's just there for now
        regex = r"([a-zA-Z0-9\_\-]+)\..*(\..+)"
        f = re.search(regex, d['filename']).groups()
        args.input = '/root/tmp/' + ''.join(f)


def download_ydl(url):
    # Set our settings
    ydl_opts = {
        # Save to /root/tmp/ and only with the ID of the video
        'outtmpl': '/root/tmp/%(id)s.%(ext)s',
        'quiet': True
    }

    print('Downloading with Youtube-DL...')
    print(url)

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        # Download the file
        ydl.download([url])
        # Get filename
        args.input = ydl.prepare_filename(ydl.extract_info(url))

    print('Download done!')


def download_torrent(url):
    print('Torrent is not yet supported')
    sys.exit(2)


def download_direct(url):
    global args
    # Get the file with a FF60 UA
    headers = { 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0' }
    print('Downloading file...')
    print(url)
    r = requests.get(url, headers=headers, allow_redirects=True)
    print('Download done!')
    # Get file name
    if r.headers.get('content-disposition'):
        args.input = '/root/tmp/' + re.findall('filename=(.+)', r.headers.get('content-disposition'))

    # if content-disposition is empty
    if args.input is None or len(args.input) == 0:
        # use last part in URL
        args.input = '/root/tmp/' + url.rsplit('/', 1)[1]

    # Write our file
    open(args.input, 'wb').write(r.content)


if __name__ == '__main__':
    args = check_args()

    if args.download:
        if 'mega.co' in args.download:
            download_mega(args.download)
        elif 'youtube.com/watch' in args.download or 'youtu.be' in args.download or \
             'dailymotion.com' in args.download or 'yandex.ru' in args.download:
            download_ydl(args.download)
        elif 'magnet:' in args.download or '.torrent' in args.download:
            download_torrent(args.download)
        elif 'http://' in args.download or 'https://' in args.download:
            download_direct(args.download)
        else:
            print('No correct URL in --download')
            exit(5)

    if not args.input or not os.path.isfile(args.input):
        print('No input file!')
        exit(6)

    # Examples of final paths
    # /var/www/vid/hls/SU/EN/MLP-04x02-EN-WEBRIP/
    # /var/www/vid/hls/MLP/EN/MLP-08x23-EN-WEBDL-YP/
    # /var/www/vid/hls/OKKO/FR/MLP-02x21-FR-TVRIP-CTOON/
    out_base = f'/var/www/vid/hls/{args.show}/{args.lang}'
    out_name = f'{args.show}-{args.season}x{args.episode}-{args.lang}-{args.profile}'

    if args.encoder:
        out_name += f'-{args.encoder}'

    cmd = f'cd /root/work && tsp -L "{out_name}" ./converter.sh "{args.input}" "{out_base}/{out_name}" {args.video_track} {args.audio_track}'
    process = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    print('Added task ' + process.stdout.decode('utf-8'))
