# Moulinette
> "(computing) A program that performs a series of simple, repetitive calculations"  
> _- Wiktionary_


## How it works

1. We give our `manager.py` details about our files and the episodes with flags.  
   If the file should be downloaded, it will get it. Otherwise it checks the file exists.
2. That script send a command to the [task spooler (`ts`)](http://vicerveza.homeunix.net/~viric/soft/ts/) 
   to start `converter.sh` with the proper arguments.
3. Each after another, videos will be encoded to HLS in multiple resolutions and bitrates.
4. ???
5. Save that into your database and start streaming.


## Todo

- [ ] Handle mega.co URLs.
- [ ] Handle Google Drive URLs.
- [ ] Handle videos with `youtube-dl`. (WIP)
- [ ] Handle torrents.
- [ ] Take/make a hash/uuid/something to prevent guessable paths.
- [ ] Be able to report back to a service if the taks succeeded or failed.
